drop database if exists phpunitdemo;
create database phpunitdemo;
use phpunitdemo;

create table person (
  id int primary key auto_increment,
  firstname varchar(50) not null,
  lastname varchar(50) not null
);

insert into person (firstname,lastname) values ('Jouni','Juntunen');