<?php
class Person {
  private $db = null;

  function  __construct() {
    try {
      $this->db = new PDO(
        'mysql:host=localhost;
        dbname=phpunitdemo;
        charset=utf8',
        'root',
        'root1234'
      );
      $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $pdoex) {
      throw new Exception(
        'Cannot create person object, check database connection details.' +
        $pdoex->getMessage()()
      );
    }
  } 

  public function save(array $data) {
    $query = $this->db->prepare('insert into person (firstname, lastname) 
    values (:firstname,:lastname)');
    $query->bindValue(':firstname',$data['firstname'],PDO::PARAM_STR);
    $query->bindValue(':lastname',$data['lastname'],PDO::PARAM_STR);
    $query->execute();
    return $this->db->lastInsertId();
  }

  public function getAll() {
    $sql="select * from person";
    return $this->db->query($sql);
  }

  public function delete(int $id) {
    $query = $this->db->prepare('delete from person where id = :id'); 
    $query->bindValue(':id',$id,PDO::PARAM_INT);
    $query->execute();
  }
}