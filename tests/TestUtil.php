<?php

class TestUtil {

  private $db = null;

  function __construct()
  {
    try {
     $this->db = new PDO('mysql:host=localhost;dbname=phpunitdemo;charset=utf8','root','root1234'); 
    }
    catch (PDOException $pdoex) {
      throw new PDOException($pdoex);
    }
  }

  public function executeInsert(string $sql) {
    $this->db->query($sql);
    return $this->db->lastInsertId();
  }

  public function executeSQL(string $sql) {
    $this->db->query($sql);
  }

  public function executeSelect(string $sql) {
    $query = $this->db->query($sql);
    return $query->fetch();
  }
}