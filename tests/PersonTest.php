<?php

/***
 * Test class for Person.
 * 
 * This class tests that database functionality on Person class is working.
 * 
 * @author Jouni Juntunen
 */

declare(strict_types=1);

require_once('TestUtil.php');
require_once('model/Person.php');

use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{

  // Object for testutil class, which provided basic functionality for manipulating test database.
  private $testUtil = null;

  /***
   * Set up this test class by creating new object of TestUtil class.
   */
  protected function setUp(): void
  {
    $this->testUtil = new TestUtil();
  }

  /***
   * Tests GetAll method on Person class.
   * 
   * Tests, that GetAll method retrieves all data available on person table.
   */
  public function testGetAll()
  {
    // Define test persons.
    $person1 = ['firstname' => 'Jouni', 'lastname' => 'Juntunen'];
    $person2 = ['firstname' => 'Pekka', 'lastname' => 'Ojala'];

    // Set up test  by deleting all person and inserting test data.
    $this->testUtil->executeSQL('delete from person');
    $person1_id = $this->testUtil->executeInsert("insert into  person (firstname,lastname) values 
    ('" . $person1['firstname'] . "','" . $person1['lastname'] . "')");

    $person2_id = $this->testUtil->executeInsert("insert into  person (firstname,lastname) values 
    ('" . $person2['firstname'] . "','" . $person2['lastname'] . "')");

    // Create person object.
    $person = new Person();
    // Call method that is tested.
    $query = $person->getAll();

    // Get first person and verify.
    $record = $this->testUtil->executeSelect("select * from person where id = " . $person1_id);
    $this->assertEquals($person1_id, $record['id']);
    $this->assertEquals($person1['firstname'], $record['firstname']);
    $this->assertEquals($person1['lastname'], $record['lastname']);

    // Get second person and verify.
    $record = $this->testUtil->executeSelect("select * from person where id = " . $person2_id);
    $this->assertEquals($person2_id, $record['id']);
    $this->assertEquals($person2['firstname'], $record['firstname']);
    $this->assertEquals($person2['lastname'], $record['lastname']);
  }

  /***
   * Tests save method (insert) on person class.
   * 
   * Tests, that save method is inserting new data to the database. 
   */
  public function testSaveInsert()
  {
    // Set data to be inserted.
    $data = [
      'firstname' => 'Jouni',
      'lastname' => 'Juntunen'
    ];

    // Create person object.
    $person = new Person;
    //Call save method that is tested.
    $id = $person->save($data);

    // Get data from database using id. Do NOT use get or other method from person class, since
    // we want our tests to be independend and isolated.
    $record = $this->testUtil->executeSelect("select * from person where id = $id");

    // Test, that data retrieved from database is the same that was inserted.
    $this->assertEquals($id, $record['id']);
    $this->assertEquals($data['firstname'], $record['firstname']);
    $this->assertEquals($data['lastname'], $record['lastname']);
  }

  public function testDelete()
  {
    // Define test persons.
    $person1 = ['firstname' => 'Jouni', 'lastname' => 'Juntunen'];

    // Set up test  by deleting all person and inserting test data.
    $this->testUtil->executeSQL('delete from person');
    $person_id = $this->testUtil->executeInsert("insert into  person (firstname,lastname) values 
    ('" . $person1['firstname'] . "','" . $person1['lastname'] . "')");

    // Create person object.
    $person = new Person();
    // Call method that is tested.
    $query = $person->delete(intval($person_id));

    // Retrieve person based on deleted id.
    $record = $this->testUtil->executeSelect("select * from person where id = $person_id");
    // Return value must be false, since record should be deleted and therefore not found.
    $this->assertFalse($record);
  }
}
