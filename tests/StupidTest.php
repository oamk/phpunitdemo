<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class StupidTest extends TestCase {
  public function testTrueIsTrue() {
    $foo = true;
    $this->assertTrue($foo);
  }
}